# Plugin WooCommerce FacturaloPerú Sunat

Público: Plugin para enviar las ventas al facturador de FacturaloPeru https://facturaloperu.com/
Repositorio Privado: https://gitlab.com/diurvan/wc-factura-facturalo

[![Plugin WooCommerce FacturaloPeru](https://gitlab.com/diurvan/plugin-woocommerce-facturaloperu-sunat/-/raw/main/Plugin_FacturaloPeru_WooCommerce.png)](https://www.youtube.com/watch?v=QyJ5POq6h5c)


Este plugin envía facturas y boletas al facturador de la Sunat, a traves de nuestra propia instalación de FacturaloPeru. Además, consulta el DNI o RUC desde la Reniec via API en el Checkout de WooCommerce.

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
